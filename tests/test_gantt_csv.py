#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
import datetime
from pathlib import Path

import pytest
import gantt

from gantt_csv import gantt_csv
from gantt_csv.gantt_csv import RESOURCES


def test_decode_resources():
    """Test possible values"""
    global RESOURCES

    resources = gantt_csv.decode_resources('None')
    assert resources is None

    resources2 = gantt_csv.decode_resources('ANO')
    assert len(resources2) == 1
    assert isinstance(resources2[0], gantt.Resource)

    resources3 = gantt_csv.decode_resources('ANO:JLS')
    assert len(resources3) == 2
    assert isinstance(resources3[0], gantt.Resource)
    assert isinstance(resources3[1], gantt.Resource)

    resources4 = gantt_csv.decode_resources('::')
    assert len(resources4) == 3
    assert isinstance(resources4[0], gantt.Resource)
    assert isinstance(resources4[1], gantt.Resource)

    RESOURCES.clear()


def test_unique_resource():
    """The same key returns the same object"""
    global RESOURCES

    resource1 = gantt_csv.get_resource('ANO')
    resource2 = gantt_csv.get_resource('ANO')
    assert id(resource1) == id(resource2)
    assert len(RESOURCES) == 1

    RESOURCES.clear()


def test_decode_start():
    """Test for possible values and other values"""
    with pytest.raises(ValueError):
        gantt_csv.decode_start('')

    assert isinstance(gantt_csv.decode_start('2014-12-25'), datetime.date)

    assert isinstance(gantt_csv.decode_start('today'), datetime.date)


def test_create_project_from_csv1():
    """Basic task example"""
    current_dir = Path(__file__).resolve().parent
    path = current_dir / "csv" / "gantt_args1.csv"
    gantt_csv.create_project_from_csv(path)


def test_create_project_from_csv2():
    """Minimal example"""
    current_dir = Path(__file__).resolve().parent
    path = current_dir / "csv" / "gantt_args2.csv"
    gantt_csv.create_project_from_csv(path)


def test_create_project_from_csv3():
    """If more than one dependent task appears in a later line"""
    current_dir = Path(__file__).resolve().parent
    path = current_dir / "csv" / "gantt_args3.csv"

    try:
        gantt_csv.create_project_from_csv(path)
    except gantt_csv.CantResolveDependencyError:
        pytest.fail("CantResolveDependencyError")


def test_create_project_from_csv4():
    """If dependent task id is not eixst"""
    current_dir = Path(__file__).resolve().parent
    path = current_dir / "csv" / "gantt_args4.csv"
    with pytest.raises(ValueError):
        gantt_csv.create_project_from_csv(path)


def test_create_project_from_table():
    """Basic task example"""
    current_dir = Path(__file__).resolve().parent
    path = current_dir / "csv" / "gantt_args1.csv"
    task_table = []
    with open(path, "rt", encoding="utf-8", newline='') as f:
        reader = csv.reader(f)
        for row in reader:
            task_table.append(row)

    gantt_csv.create_project_from_table(
        "Example project",
        task_table[1:],
        header=task_table[0]
    )
