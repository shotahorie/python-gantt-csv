#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
import datetime

import gantt

from gantt_csv import create_project_from_table, RESOURCES


YMD_VACATIONS = [
    (2014, 12, 30),
    (2014, 12, 31),
    (2015, 1, 1),
    (2015, 1, 2),
]


# Change font default
gantt.define_font_attributes(fill='black',
                             stroke='black',
                             stroke_width=0,
                             font_family="Verdana")

# Add vacations for everyone
for year, month, date in YMD_VACATIONS:
    gantt.add_vacations(datetime.date(year, month, date))

# Create project from arguments table
projects = []

task_table = []
with open("gantt_args1.csv", "rt", encoding="utf-8", newline='') as f:
    reader = csv.reader(f)
    for row in reader:
        task_table.append(row)

p1 = create_project_from_table(
    "Example2 project",
    task_table[1:],
    header=task_table[0]
)
projects.append(p1)

# Create parent project
parent_project = gantt.Project(name='Parent Project')
# which contains the other projects
for project in projects:
    parent_project.add_task(project)

# MAKE DRAW
parent_project.make_svg_for_tasks(filename='test_full.svg',
                                  today=datetime.date.today(),
                                  start=datetime.date(2014, 12, 20),
                                  end=datetime.date(2015, 2, 20))
parent_project.make_svg_for_resources(filename='test_resources.svg',
                                      today=datetime.date.today(),
                                      resources=tuple(RESOURCES.values()))
parent_project.make_svg_for_tasks(filename='test_weekly.svg',
                                  today=datetime.date.today(),
                                  scale=gantt.DRAW_WITH_WEEKLY_SCALE)
